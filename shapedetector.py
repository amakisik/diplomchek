# -*- coding: utf8 -*-

# import the necessary packages
import cv2
import math
import numpy as np
import cv2 as cv


class ShapeDetector:
    def __init__(self):
        pass

    def detect(self, c):
        shape = "unidentified"
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)

        if 3 < len(approx):
            shape = "circle"
        return shape