# -*- coding: utf8 -*-

import random
import sys  # sys нужен для передачи argv в QApplication
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap

from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QInputDialog, QAction, QFileDialog, QMessageBox

import untitled  # Это наш конвертированный файл дизайна
import detect_shapes
import cv2

import numpy as np

import os
import sys  # sys нужен для передачи argv в QApplication
import time

window = None
connectLine_cs = 0
start_pos = 0

editFileName = None


class ImageEditorForm(QtWidgets.QMainWindow, untitled.Ui_MainWindow):
    def __init__(self, parent):
        super(ImageEditorForm, self).__init__(parent)

        global editFileName

        self.editFileName = editFileName
        self.start_pos = None
        self.is_line_drawing = True

        self.image = QPixmap(editFileName)
        self.image_original = self.image.copy()

        self.setFixedSize(self.image.size())
        self.setWindowTitle("Image editor")

        main_menu = self.menuBar()
        main_menu.setStyleSheet("""QMenu::item::selected { color: rgb(45, 45, 48); }""")
        file_menu = main_menu.addMenu("Файл")

        clear_action = QAction("Очистить", self)
        clear_action.setShortcut("Ctrl+R")
        main_menu.addAction(clear_action)
        clear_action.triggered.connect(self.clear_drawings)

        save_action = QAction("Сохранить", self)
        save_action.setShortcut("Ctrl+S")
        file_menu.addAction(save_action)
        save_action.triggered.connect(self.save)

        self.btn1 = QtWidgets.QPushButton(self)
        self.btn1.setGeometry(QtCore.QRect(20, 20, 100, 30))
        self.btn1.setText("Добавить вершину")
        self.btn1.clicked.connect(self.set_draw_circles)
        self.btn1.setStyleSheet("font: 75 8pt \"Calibri\";\n"
                                "color: rgb(237, 236, 236);\n"
                                "background-color: rgb(89, 87, 88);\n"
                                "border-radius: 5px;")

        self.btn2 = QtWidgets.QPushButton(self)
        self.btn2.setGeometry(QtCore.QRect(150, 20, 100, 30))
        self.btn2.setText("Добавить ребро")
        self.btn2.clicked.connect(self.set_draw_lines)
        self.btn2.setStyleSheet("font: 75 8pt \"Calibri\";\n"
                                "color: rgb(237, 236, 236);\n"
                                "background-color: rgb(89, 87, 88);\n"
                                "border-radius: 5px;")

    def save(self):
        filePath, _ = QFileDialog.getSaveFileName(self, "Save Image", "",
                                                  "PNG(*.png);;JPEG(*.jpg *.jpeg);;All Files(*.*) ")

        if filePath == "":
            return
        self.image.save(filePath)

        self.close()
        self.parent().OpenFolder(filePath)

    def set_draw_lines(self):
        QMessageBox.about(self, "Информация", "Выбран инструмент \"Ребро\"")
        self.is_line_drawing = True

    def set_draw_circles(self):
        QMessageBox.about(self, "Информация", "Выбран инструмент \"Вершина\"")
        self.is_line_drawing = False

    def paintEvent(self, e: QtGui.QPaintEvent) -> None:
        painter = QPainter(self)
        painter.drawPixmap(self.rect(), self.image)

        self.image_original = QPixmap(self.editFileName)

        self.update()

    def mousePressEvent(self, e):
        if e.button() == Qt.LeftButton:
            if self.is_line_drawing:
                self.line_drawing(e)
            else:
                self.circle_drawing(e)
                pass

        if e.button() == Qt.RightButton:
            print("clear")
            self.clear_drawings()

    def line_drawing(self, e):
        if not self.start_pos:
            self.start_pos = e.pos()
        else:
            paint = QPainter(self.image)
            pen = QtGui.QPen(QtCore.Qt.black, 1, QtCore.Qt.SolidLine)
            paint.setPen(pen)

            paint.drawLine(self.start_pos, e.pos())
            paint.drawLine(self.start_pos, e.pos())

            self.start_pos = None

    def circle_drawing(self, e):
        paint = QPainter(self.image)

        paint.setBrush(QColor('black'))
        paint.drawEllipse(e.pos(), 25, 25)

    def clear_drawings(self):
        self.image = self.image_original
        self.start_pos = None


class rename_node_dialog(QtWidgets.QDialog):
    def __init__(self, parent):
        super(rename_node_dialog, self).__init__(parent)

        self.ok = False

        self.btn_ok = QtWidgets.QPushButton("Rename", self)
        self.btn_ok.clicked.connect(self.button_press)
        self.btn_cancel = QtWidgets.QPushButton("Delete", self)
        self.btn_cancel.clicked.connect(self.button_press)
        self.btn_cancel.move(80, 0)

    def button_press(self):
        if self.sender() == self.btn_ok:
            self.ok = True
        self.close()

    @classmethod
    def isOkay(cls, parent):
        dialog = cls(parent)
        dialog.exec_()
        return dialog.ok


class main_form(QtWidgets.QMainWindow, untitled.Ui_MainWindow):
    def __init__(self):
        # Это здесь нужно для доступа к переменным, методам
        # и т.д. в файле untitled.py
        super().__init__()

        # self.bbtn = QtWidgets.QPushButton(self)
        # self.bbtn.setGeometry(QtCore.QRect(150, 10, 56, 17))
        # self.bbtn.setObjectName("bbtn")

        self.setupUi(self)  # Это нужно для инициализации нашего дизайна
        self.btnLoadPhoto.clicked.connect(self.OpenFolder)
        # self.btnLoadPhoto.clicked.connect(self.open_image)

        self.pos = None
        self.start_pos = None

        self.vertex_buttons_array = []

        # region Pixmap settings
        # painter = QPainter(self)
        # pen = QtGui.QPen(QtCore.Qt.black, 2, QtCore.Qt.SolidLine)
        # painter.setPen(pen)
        # pixmap = QPixmap(50, 50)
        # pixmap.fill(Qt.white)
        # painter.begin(pixmap)
        # painter.drawLine(0, 0, 50, 500)
        # painter.end()

        #self.labelImage.setPixmap(pixmap)
        # endregion
        # region Menu bar

        self.extractAction = QAction("&Редактировать", self)

        # extractAction.setShortcut("Ctrl+Q")
        self.extractAction.setStatusTip('Edit image')
        self.extractAction.setEnabled(False)
        self.extractAction.triggered.connect(self.open_image_edit)

        self.statusBar()

        mainMenu = self.menuBar()
        mainMenu.setStyleSheet("""QMenu::item::selected { color: rgb(45, 45, 48); }""")
        fileMenu = mainMenu.addMenu('&Файл')
        fileMenu.addAction(self.extractAction)

        saveMenu = mainMenu.addMenu('&Сохранить как')
        self.saveAdjacencyMatrixAction = QAction("&Матрица смежности", self)
        self.saveAdjacencyMatrixAction.setStatusTip('adjacency matrix')
        self.saveAdjacencyMatrixAction.triggered.connect(self.save_adjacency_matrix)
        saveMenu.addAction(self.saveAdjacencyMatrixAction)

        self.saveEdgesAction = QAction("&Список рёбер", self)
        self.saveEdgesAction.setStatusTip('list of edges')
        self.saveEdgesAction.triggered.connect(self.save_list_edges)

        saveMenu.addAction(self.saveEdgesAction)


        self.saveAction = QAction("&Сохранить", self)

        # endregion

        # self.clickMethod()

    def save_adjacency_matrix(self):
        graph = {
            '1': ['2', '6'],
            '2': ['1', '4'],
            '3': ['6'],
            '4': ['2', '8'],
            '5': ['6', '7'],
            '6': ['1', '3', '5', '8'],
            '7': ['5', '9', '10'],
            '8': ['4', '6', '9'],
            '9': ['7', '8'],
            '10': ['7']
        }
        n = 10

        matrix = [[0] * n for _ in range(n)]

        for vertex_y, value in graph.items():
            for vertex_x in value:
                matrix[int(vertex_y) - 1][int(vertex_x) - 1] = 1

        a = '     '
        for i in range(n):
            a += f"{i}, "

        f = open("матрица_смежности.txt", "w")
        file_text = a
        f.write(a + "\n")
        for i, row in enumerate(matrix):
            f.write(f'{i + 1} - {row}\n')
            # print(f'{i + 1} - {row}')
        f.close()
        QMessageBox.about(self, "Успешно", "Сохранено прошло успешно")

    def save_list_edges(self):
        graph = {
            '1': ['2', '6'],
            '2': ['1', '4'],
            '3': ['6'],
            '4': ['2', '8'],
            '5': ['6', '7'],
            '6': ['1', '3', '5', '8'],
            '7': ['5', '9', '10'],
            '8': ['4', '6', '9'],
            '9': ['7', '8'],
            '10': ['7']
            
        }
        with open('список_рёбер.txt', 'w') as out:
            for key, val in graph.items():
                out.write('{}:{}\n'.format(key, val))

        QMessageBox.about(self, "Успешно", "Сохранено прошло успешно")


    def open_image_edit(self):
        self.dialog.show()

    # def prees_node(self, event):
    #     painter = QPainter(self)
    #
    #
    #     pixmap = QPixmap("lines.jpg")
    #     painter.drawPixmap(self.rect(), pixmap)
    #     pen = QPen(Qt.red, 3)
    #     painter.setPen(pen)
    #     painter.drawLine(10, 10, self.rect().width() - 10, 10)

    def node_layout(self, event):
        global connectLine_cs
        if connectLine_cs == 0:
            self.start_pos = event.pos()
            connectLine_cs = 1

        distance_from_center = round(((event.y() - 250) ** 2 + (event.x() - 500) ** 2) ** 0.5)
        self.label.setText(
            'Coordinates: ( %d : %d )' % (event.x(), event.y()) + "Distance from center: " + str(distance_from_center))
        self.pos = event.pos()
        self.update()

    def prees_node(self, event):
        if self.pos and connectLine_cs == 1:
            q = QPainter(self)
            #
            painter = QPainter(self)
            pen = QtGui.QPen(QtCore.Qt.black, 2, QtCore.Qt.SolidLine)
            painter.setPen(pen)
            pixmap = QPixmap(350, 350)
            pixmap.fill(Qt.white)
            painter.begin(pixmap)
            painter.drawLine(self.start_pos.x(), self.start_pos.y(), self.pos.x(), self.pos.y())
            painter.drawLine(0, 0, 50, 500)
            painter.end()

            painter = QPainter(self)
            pen = QtGui.QPen(QtCore.Qt.black, 2, QtCore.Qt.SolidLine)
            painter.setPen(pen)
            pixmap = QPixmap(50, 50)
            pixmap.fill(Qt.white)
            painter.begin(pixmap)
            painter.drawLine(0, 0, 50, 500)
            painter.end()

    def clickMethod(self, x, y, name):
        x += 7
        y += 67
        print('Clicked')
        newBtn = QtWidgets.QPushButton(str(name), self)
        newBtn.setStyleSheet("font: 75 8pt \"Calibri\";\n"
                             "color: rgb(237, 236, 236);\n"
                             "background-color: rgb(236, 102, 37);\n"
                             "border-radius: 15px;")
        # newBtn.setStyleSheet("background-color: transparent;")
        newBtn.setFixedWidth(30)
        print(f"{x} - {y}")
        newBtn.move(x, y)
        btn_name = 'btn' + str(name)
        self.vertex_buttons_array.append(newBtn)

        newBtn.setObjectName(btn_name)
        newBtn.show()

        newBtn.clicked.connect(lambda: self.open_image(newBtn))

    def open_image(self, newBtn):
        text, ok = QInputDialog.getText(self, 'Input Dialog',
                                        'Введите номер точки:')
        if ok:
            newBtn.setText(str(text))
        else:
            return

    # def rename_circle(self):
    #     print(rename_node_dialog.isOkay(self))

    def OpenFolder(self, filePath):
        if not filePath:
            directory = QtWidgets.QFileDialog.getOpenFileName(self, "Выберите папку")
            directory = directory[0]
        else:
            directory = filePath


        if not directory:
            return

        if self.vertex_buttons_array:
            for i, btn in enumerate(self.vertex_buttons_array):
                btn.deleteLater()

        self.vertex_buttons_array = []

        self.extractAction.setEnabled(True)
        print(directory)
        global editFileName

        image = cv2.imread(directory)
        image = cv2.resize(image, (611, 751))
        # image = image
        path = directory.split("/")
        print(path[-1])
        cv2.imwrite(path[-1], image)
        editFileName = path[-1]
        detect_shapes.detect_circle(directory, window)

        self.dialog = ImageEditorForm(self)

        pixmap = QtGui.QPixmap("lines.jpg")
        self.labelImage.setPixmap(pixmap)


        # self.labelImage.resize(pixmap.width(), pixmap.height())
        # self.resize(pixmap.width(), pixmap.height())


def main():
    global window
    app = QtWidgets.QApplication(sys.argv)
    window = main_form()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()

# pyuic5 design.ui -o untitled.py
