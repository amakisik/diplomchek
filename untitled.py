# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(649, 813)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("background-color: rgb(245, 245, 245);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnLoadPhoto = QtWidgets.QPushButton(self.centralwidget)
        self.btnLoadPhoto.setGeometry(QtCore.QRect(20, 10, 100, 28))
        self.btnLoadPhoto.setStyleSheet("font: 75 8pt \"Calibri\";\n"
"color: rgb(237, 236, 236);\n"
"background-color: rgb(89, 87, 88);\n"
"border-radius: 5px;")
        self.btnLoadPhoto.setObjectName("btnLoadPhoto")
        self.labelImage = QtWidgets.QLabel(self.centralwidget)
        self.labelImage.setGeometry(QtCore.QRect(20, 50, 611, 751))
        self.labelImage.setMouseTracking(False)
        self.labelImage.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.labelImage.setAutoFillBackground(False)
        self.labelImage.setStyleSheet("#labelImage{\n"
"background-color: rgb(255, 255, 255);\n"
"}")
        self.labelImage.setText("")
        self.labelImage.setScaledContents(True)
        self.labelImage.setObjectName("labelImage")
        self.labelImage.raise_()
        self.btnLoadPhoto.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnLoadPhoto.setWhatsThis(_translate("MainWindow", "<html>\n"
"<head/>\n"
"<body>\n"
"<p><br/></p>\n"
"</body>\n"
"</html>"))
        self.btnLoadPhoto.setText(_translate("MainWindow", "Load Image"))
