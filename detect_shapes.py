# -*- coding: utf8 -*-

# import the necessary packages
from itertools import groupby

from PyQt5.QtWidgets import QPushButton, QWidget

from shapedetector import ShapeDetector
from PyQt5 import QtCore, QtGui, QtWidgets
import argparse
import imutils
import cv2
import numpy as np
import main

# construct the argument parse and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-i", "--image", required=True,
#                help="path to the input image")
# args = vars(ap.parse_args())

# load the image and resize it to a smaller factor so that
# the shapes can be approximated better

all_lines = None
all_circles = None


def detect_lines(filename):
    # Reading the required image in
    # which operations are to be done.
    # Make sure that the image is in the same
    # directory in which this python program is
    # img = filename
    img = cv2.imread(filename)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray, 100, 200)
    line_image = np.copy(img) * 0

    # blurred = cv2.blur(edges, (5, 5))
    lines = cv2.HoughLinesP(edges, 1, np.pi / 200, 60, minLineLength=50, maxLineGap=30)

    # cv2.imshow("Lines Edges", edges)
    # cv2.waitKey(1)

    # print(lines)

    if lines is None:
        print("Zero lines")
        img = cv2.resize(img, (611, 751))
        cv2.imwrite("lines.jpg", img)
        return

    # The below for loop runs till r and theta values
    # are in the range of the 2d array

    all_lines = lines

    lines_array = []
    except_i = []
    except_j = []
    shadow_layer = []

    dd = {}
    lines_all_ids = []
    for i in range(len(lines)):
        lines_all_ids.append(i)
        line_f = lines[i]
        if i in except_i: continue
        x1_f, y1_f, x2_f, y2_f = 0, 0, 0, 0
        for x1_f, y1_f, x2_f, y2_f in line_f:
            x1_f, y1_f, x2_f, y2_f = x1_f, y1_f, x2_f, y2_f

        angle_f = np.arctan2([y1_f, y2_f], [x1_f, x2_f]) * 180 / np.pi

        for j in range(len(lines)):
            if i == j: continue
            if j >= len(lines): break
            if j in except_j: continue
            line_s = lines[j]
            x1_s, y1_s, x2_s, y2_s = 0, 0, 0, 0
            for x1_s, y1_s, x2_s, y2_s in line_s:
                x1_s, y1_s, x2_s, y2_s = x1_s, y1_s, x2_s, y2_s
            angle_s = np.arctan2([y1_s, y2_s], [x1_s, x2_s]) * 180 / np.pi

            diff_x1 = np.abs(x1_f - x1_s)
            diff_x2 = np.abs(x2_f - x2_s)
            diff_y1 = np.abs(y1_f - y1_s)
            diff_y2 = np.abs(y2_f - y2_s)
            diff_val = 50
            if (diff_x1 <= diff_val) and (diff_x2 <= diff_val) and (diff_y1 <= diff_val) and (diff_x2 <= diff_val):
                diff_angle = np.abs(angle_f - angle_s)
                print(diff_angle)
                except_i.append(i)
                except_j.append(j)
                tmp = []
                if j in dd:
                    continue
                if i in dd:
                    if j == dd[i]:
                        continue
                    if isinstance(dd[i], list):
                        dd[i].append(j)
                    else:
                        dd[i] = [dd[i], j]
                else:
                    dd[i] = j

            # print(f"line {i}) - x1={x1_f}, y1={y1_f}, x2={x2_f}, y2={y2_f}\n")
            # print(f"line {j}) - x1={x1_s}, y1={y1_s}, x2={x2_s}, y2={y2_s}\n")
        # print(f'atan2 - {np.arctan2([y1_f, y2_f], [x1_f, x2_f]) * 180 / np.pi}')

    lines_ids = []
    shadow_layer = except_i + except_j
    lines_ids = list(set(shadow_layer))

    for key, value in dd.items():
        if isinstance(value, list):
            for item in value:
                lines_all_ids.remove(item)
        else:
            lines_all_ids.remove(value)

            # if isinstance(value, list):
            #     for item in value:
            #         if item == lines_all_ids[j]:
            #             lines_all_ids.remove(lines_all_ids[j])
            # elif key == lines_all_ids[j]:
            #     lines_all_ids.remove(lines_all_ids[j])

    # lines_all_ids.append(2)
    # lines_all_ids.append(4)
    # lines_all_ids.append(2)
    # Drawing
    for k, i in enumerate(lines_all_ids):
        # if k != 3: continue
        # if k >= 4:
        #     break
        line = lines[i]
        x1, y1, x2, y2 = 0, 0, 0, 0
        for x1, y1, x2, y2 in line:
            x1, y1, x2, y2 = x1, y1, x2, y2

        angle = np.arctan2([y1, y2], [x1, x2]) * 180 / np.pi

        lines_array.append(((x1 + 0.0, y1 + 0.0), (x2 + 0.0, y2 + 0.0)))
        cv2.line(line_image, (x1, y1), (x2, y2), (0, 0, 255), 5)
        print(f"line {k}) - x1={x1}, y1={y1}, x2={x2}, y2={y2}, angle={angle}\n")

    # for i in range(len(lines)):
    #     if i not in lines_ids:
    #         lines_ids.append(i)

    print(f'array: {lines_all_ids}')

    print(f'dd: {dd}')
    print(f'shadow layer {lines_ids}')
    print(f'except i {except_i}')
    print(f'except j {except_j}')

    # for i, line in enumerate(lines):
    #     for x1, y1, x2, y2 in line:
    #         print(line)
    #         f = open("data.txt", "a")
    #         f.write(f"line {i}) - x1={x1}, y1={y1}, x2={x2}, y2={y2}\n")
    #         print(f"line {i}) - x1={x1}, y1={y1}, x2={x2}, y2={y2}\n")
    #
    #         lines_array.append(((x1 + 0.0, y1 + 0.0), (x2 + 0.0, y2 + 0.0)))
    #         cv2.line(line_image, (x1, y1), (x2, y2), (0, 0, 255), 5)
    #         f.close()

    lines_edges = cv2.addWeighted(img, 0.8, line_image, 1, 0)

    print(f"{len(lines_all_ids)} - lines")
    # cv2.imshow("asd", lines_edges)

    # cv2.imshow("Lines", img)
    # key = cv2.waitKey(1)
    # if key == 27:
    #     return
    # All the changes made in the input image are finally
    # written on a new image houghlines.jpg
    img = cv2.resize(img, (668, 820))
    cv2.imwrite("lines.jpg", lines_edges)


def detect_circle(filename, window):
    out_file_name = "out.jpg"
    image = cv2.imread(filename)
    resized = imutils.resize(image)
    ratio = image.shape[0] / float(resized.shape[0])

    # convert the resized image to grayscale, blur it slightly,
    # and threshold it
    gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
    blurred = cv2.blur(gray, (5, 5))
    thresh = cv2.threshold(blurred, 56, 255, cv2.THRESH_BINARY_INV)[1]  # 60, 255,
    # image colors invert
    # thresh = cv2.bitwise_not(thresh)

    # cv2.imshow("Circle thresh", thresh)
    # cv2.waitKey(0)
    # cv2.imwrite("out1.png", thresh)

    # find contours in the thresholded image and initialize the
    # shape detector
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    sd = ShapeDetector()

    z = 0
    f = open("data.txt", "w")

    all_circles = cnts

    for c in cnts:
        # compute the center of the contour, then detect the name of the
        # shape using only the contour
        M = cv2.moments(c)

        try:
            cX = int((M["m10"] / M["m00"]) * ratio)
            cY = int((M["m01"] / M["m00"]) * ratio)
            shape = sd.detect(c)
            # if shape == "circle":
            #	z = z+1
            if shape == 'unidentified':
                continue

            # multiply the contour (x, y)-coordinates by the resize ratio,
            # then draw the contours and the name of the shape on the image
            c = c.astype("float")
            c *= ratio
            c = c.astype("int")
            cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
            z = z + 1
            cv2.circle(image, (cX, cY), 1, (54, 54, 199), 10)

            window.clickMethod(cX, cY, z)

            # Номера
            # cv2.putText(image, str(z), (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
            f.write(f"point {z}) - cX={cX}, cY={cY}\n")

            #
            # show the output image
            # cv2.imshow("Image", image)
            # cv2.imwrite("out.png", image)
            # cv2.waitKey(0)
        except Exception as e:
            print(e)

    f.close()
    image = cv2.resize(image, (611, 751))
    # image = image
    cv2.imwrite(out_file_name, image)
    liens_array = detect_lines("out.jpg")
    # cv2.imshow("Image", image)
    # cv2.waitKey(0)

    print(z)

    # python detect_shapes.py --image shapes_and_colors.png
